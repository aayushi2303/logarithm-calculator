#include <iostream>
#include <math.h>
#include <cmath>
#include <vector>
#include <errno.h>
#include <fstream>
#include <iomanip>
using namespace std;

void logarithm1(double, double, double, double);
void logarithm_of_number(double, double, double, double, double);
ofstream fout;
int iteration = 1;


int main(){

	fout.open("LogTable.txt");
	fout << "Iter\tLowestNumber\tHighestNumber\tnewNumber\t\tlogLowest\tlogHighest\tLogNewNumber" << endl;

	logarithm1(100.0, 1000.0, 2.0, 3.0);
			
	//system("pause");
	return 0;
}



void logarithm1(double a, double b, double logA, double logB){

	if (abs(logA - logB) <= 0.000001){
		iteration++;
		return;		
	}
	else {

		double K = std::sqrt(a*b);
		double logK = (logA+logB)/2;
	
		fout << setw(15) << setprecision(0) << (iteration/2)+1 << "\t";
		fout << setw(15) << setprecision(10) << a << "\t";
		fout << setw(15)<< setprecision(10) << b << "\t";
		fout << setw(15)<< setprecision(10) << K << "\t";
		fout << setw(15)<< setprecision(10) << logA << "\t";
		fout << setw(15)<< setprecision(10) << logB << "\t";
		fout << setw(15)<< setprecision(10) << logK << endl;
		
		logarithm1(a, K, logA, logK);
		logarithm1(K, b, logK, logB);
	}	
}
