#include <iostream>
#include <math.h>
#include <cmath>
#include <vector>
#include <errno.h>
#include <fstream>
#include <iomanip>
using namespace std;

void logarithm_of_number2(double, double, double, double, double);
ofstream fout;


int main(){

	fout.open("LogTable3.txt");
	fout << "LowestNumber\tHighestNumber\tnewNumber\t\tlogLowest\tlogHighest\tLogNewNumber" << endl;

	logarithm_of_number2(3000000, 100, 1000, 2, 3);
		
	return 0;
}

void logarithm_of_number2(double target, double min, double max, double minLog, double maxLog){
	double K = min*max;
	double logK = (minLog + maxLog)/2.0;

	
	if (abs(minLog - maxLog) <= 0.000001){
		cout << "Log of " << target << " is: " << minLog << endl;
		return;		
	}
   
   		
	fout << setw(15) << setprecision(10) << min<< "\t";
	fout << setw(15)<< setprecision(10) << max << "\t";
	fout << setw(15)<< setprecision(10) << K << "\t";
	fout << setw(15)<< setprecision(10) << minLog << "\t";
	fout << setw(15)<< setprecision(10) << maxLog << "\t";
	fout << setw(15)<< setprecision(10) << logK << endl;
   
    if (target >= min && target < sqrt(K)){
		logarithm_of_number2(target, min, sqrt(K), minLog, logK);
	}
	else if (target > sqrt(K) && target <= max){
		logarithm_of_number2(target, sqrt(K), max, logK, maxLog);
	}	
	else if(target < min){
		logarithm_of_number2(target, pow(10, minLog-1), min, minLog-1, minLog);
	}		
	else if(target > max){
		logarithm_of_number2(target, max, pow(10, maxLog+1), maxLog, maxLog+1);

	}
	
}

